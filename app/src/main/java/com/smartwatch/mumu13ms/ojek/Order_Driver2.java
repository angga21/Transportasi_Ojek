package com.smartwatch.mumu13ms.ojek;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Order_Driver2 extends AppCompatActivity {
    Button btnPickup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order__driver2);

        btnPickup = (Button) findViewById(R.id.btnPickup);

        btnPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Order_Driver3.class);
                startActivity(i);
            }
        });
    }
}
