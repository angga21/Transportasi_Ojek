package com.smartwatch.mumu13ms.ojek;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    EditText uname, pass;
    Button btnLogin, btnRegister, btnDriver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        uname = (EditText) findViewById(R.id.username);
        pass = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnDriver = (Button) findViewById(R.id.btnDriver);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a = String.valueOf(uname.getText());
                String b = String.valueOf(pass.getText());
                if(a.equals("user") && b.equals("user")){
                    Intent i = new Intent(getApplicationContext(), Antar_jemput.class);
                    startActivity(i);
                }else if (a.equals("driver") && b.equals("driver")){
                    Intent i = new Intent(getApplicationContext(), Order_Driver.class);
                    startActivity(i);
                }else {
                    Toast.makeText(getApplication(), "Username & Password Salah!", Toast.LENGTH_LONG).show();
                }

            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent j = new Intent(getApplicationContext(), Register.class);
                startActivity(j);
            }
        });

        btnDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent j = new Intent(getApplicationContext(), Register_Driver.class);
                startActivity(j);
            }
        });
    }
}
