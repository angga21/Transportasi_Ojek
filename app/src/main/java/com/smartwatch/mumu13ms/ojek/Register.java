package com.smartwatch.mumu13ms.ojek;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Register extends AppCompatActivity {
    EditText nama, email, pass, kpass, nope, kota;
    Button btnBatal, btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        nama = (EditText) findViewById(R.id.nama);
        email = (EditText) findViewById(R.id.email);
        pass = (EditText) findViewById(R.id.psw);
        kpass = (EditText) findViewById(R.id.kpsw);
        nope = (EditText) findViewById(R.id.nope);
        kota = (EditText) findViewById(R.id.kota);
        btnRegister = (Button) findViewById(R.id.btnregis);
        btnBatal = (Button) findViewById(R.id.btnbatal);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Login.class);
                startActivity(i);
            }
        });

        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent j = new Intent(getApplicationContext(), Login.class);
                startActivity(j);
            }
        });
    }
}
