package com.smartwatch.mumu13ms.ojek;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Cekpoint extends AppCompatActivity {
    Button btnSampai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cekpoint);

        btnSampai = (Button) findViewById(R.id.btnsampai);

        btnSampai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), FeedBack.class);
                startActivity(i);
            }
        });
    }
}
