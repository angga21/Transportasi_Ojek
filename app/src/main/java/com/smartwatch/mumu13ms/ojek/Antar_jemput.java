package com.smartwatch.mumu13ms.ojek;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Antar_jemput extends AppCompatActivity {
    EditText Lawal, Ltujuan;
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_antar_jemput);

        Lawal = (EditText) findViewById(R.id.lokasiawal);
        Ltujuan = (EditText) findViewById(R.id.lokasitujuan);
        btnSubmit = (Button) findViewById(R.id.btnsumbit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Pembayaran.class);
                startActivity(i);
            }
        });
    }
}
