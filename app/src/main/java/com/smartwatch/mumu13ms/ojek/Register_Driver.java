package com.smartwatch.mumu13ms.ojek;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Register_Driver extends AppCompatActivity {
    EditText nama, email, pass, kota, status, ttl, sim;
    Button btnRegister, btnBatal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register__driver);

        nama = (EditText) findViewById(R.id.nama);
        email = (EditText) findViewById(R.id.email);
        pass = (EditText) findViewById(R.id.password);
        kota = (EditText) findViewById(R.id.kota);
        status = (EditText) findViewById(R.id.status);
        ttl = (EditText) findViewById(R.id.ttl);
        sim = (EditText) findViewById(R.id.sim);
        btnBatal = (Button) findViewById(R.id.btnBatal);
        btnRegister = (Button) findViewById(R.id.btnRegister);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent j = new Intent(getApplicationContext(), Login.class);
                startActivity(j);
            }
        });

        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent j = new Intent(getApplicationContext(), Login.class);
                startActivity(j);
            }
        });
    }
}
